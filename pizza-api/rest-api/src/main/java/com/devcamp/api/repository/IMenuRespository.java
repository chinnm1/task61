package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.CMenu;

public interface IMenuRespository extends JpaRepository<CMenu, Long> {

}
