package com.devcamp.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.CDrink;
import com.devcamp.api.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired
    IDrinkRepository pCustomerRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks() {
        try {
            List<CDrink> listCustomer = new ArrayList<CDrink>();
            pCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
