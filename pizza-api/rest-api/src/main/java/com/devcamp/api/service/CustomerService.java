package com.devcamp.api.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.api.model.CCustomer;
import com.devcamp.api.model.COrder;
import com.devcamp.api.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;

    public ArrayList<CCustomer> getAllCars() {
        ArrayList<CCustomer> listCar = new ArrayList<>();
        pCustomerRepository.findAll().forEach(listCar::add);
        return listCar;
    }

    public Set<COrder> getOrderByCustomerId(long customerId) {
        /*
         * List<CRegion> pRegions = new ArrayList<CRegion>();
         * pRegionRepository.findAll().forEach(pRegions::add);
         */
        CCustomer vCustomer = pCustomerRepository.findByCustomerId(customerId);
        if (vCustomer != null) {
            return vCustomer.getOrders();
        } else {
            return null;
        }
    }

}
