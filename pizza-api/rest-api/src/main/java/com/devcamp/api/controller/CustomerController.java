package com.devcamp.api.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.CCustomer;
import com.devcamp.api.model.COrder;
import com.devcamp.api.service.CustomerService;
import com.devcamp.api.service.OrderService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCars() {
        try {
            return new ResponseEntity<>(customerService.getAllCars(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lấy danh sách order truyền vào country code /order?carId=...
    @GetMapping("/order")
    public ResponseEntity<Set<COrder>> getOrderByCustomerId(
            @RequestParam(value = "customerId") long customerId) {
        try {
            Set<COrder> order = customerService.getOrderByCustomerId(customerId);
            if (order != null) {
                return new ResponseEntity<>(order, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order-all")
    public ResponseEntity<List<COrder>> getorderAll() {
        try {
            return new ResponseEntity<>(orderService.getAllOrder(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}