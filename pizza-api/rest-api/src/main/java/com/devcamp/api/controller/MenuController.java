package com.devcamp.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.model.CMenu;

import com.devcamp.api.repository.IMenuRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class MenuController {
    @Autowired
    IMenuRespository pMenuRepository;

    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getAllMenu() {
        try {
            List<CMenu> listMenu = new ArrayList<CMenu>();
            pMenuRepository.findAll().forEach(listMenu::add);
            return new ResponseEntity<>(listMenu, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
