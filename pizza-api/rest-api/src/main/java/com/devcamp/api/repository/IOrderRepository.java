package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {

}
