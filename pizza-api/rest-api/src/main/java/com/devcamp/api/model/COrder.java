package com.devcamp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    @Column(name = "order_code")
    private String orderCode;

    @Column(name = "pizza_size")
    private String pizzaSize;
    @Column(name = "voucher_code")
    private String voucherCode;

    @Column(name = "price")
    private long price;
    @Column(name = "paid")
    private long paid;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "customer_id")
    private CCustomer customer;

    public COrder(long orderId, String orderCode, String pizzaSize, String voucherCode, long price, long paid,
            CCustomer customer) {
        this.orderId = orderId;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
        this.customer = customer;
    }

    public COrder() {
    }

    public long getOrderId() {
        return orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public long getPrice() {
        return price;
    }

    public long getPaid() {
        return paid;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setPaid(long paid) {
        this.paid = paid;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }
}
