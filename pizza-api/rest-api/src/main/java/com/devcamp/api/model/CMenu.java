package com.devcamp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "size")
    private char size;
    @Column(name = "duong_kinh")
    private int duongKinh;
    @Column(name = "suon")
    private int suon;
    @Column(name = "salad")
    private int salad;
    @Column(name = "so_luong_nuoc_ngot")
    private int soLuongNuocNgot;
    @Column(name = "don_gia")
    private int donGia;

    public CMenu(long id, char size, int duongKinh, int suon, int salad, int soLuongNuocNgot, int donGia) {
        this.id = id;
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuocNgot = soLuongNuocNgot;
        this.donGia = donGia;
    }

    public CMenu() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getSoLuongNuocNgot() {
        return soLuongNuocNgot;
    }

    public void setSoLuongNuocNgot(int soLuongNuocNgot) {
        this.soLuongNuocNgot = soLuongNuocNgot;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
}
